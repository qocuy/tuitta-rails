class CreateTweets < ActiveRecord::Migration[5.0]
  def change
    create_table :tweets do |t|
      t.text :text, null: false
      t.datetime :created_at, null: false
    end
  end
end
