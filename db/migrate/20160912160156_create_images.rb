class CreateImages < ActiveRecord::Migration[5.0]
  def change
    create_table :images do |t|
      t.integer :tweet_id, null: false
      t.string :mime_type
      t.binary :data, null: false
      t.datetime :created_at, null: false
    end
  end
end
