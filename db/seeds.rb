# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
tweets = File.open('/home/user/.tuitta/statuses.json', 'r') do |file|
  file.each_line.map {|line| JSON.parse(line, symbolize_names: true) }
end

ActiveRecord::Base.record_timestamps = false

tweets.each do |tweet|
  tweet = Tweet.create(text: tweet[:text], created_at: Time.at(tweet[:created_at]).to_datetime)
end
