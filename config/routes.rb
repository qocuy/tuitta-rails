Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'timeline#index'
  get '/login', to: 'auth#login'
  post '/login', to: 'auth#login'
  get '/logout', to: 'auth#logout'
  get '/images/:id', to: 'images#show'
  get '/api/timeline', to: 'timeline#timeline'
  post '/api/search', to: 'timeline#search'
  get '/api/date/:date_str', to: 'timeline#date'
  post '/api/create', to: 'timeline#create'
end
