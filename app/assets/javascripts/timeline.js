function createEntry(date) {
  var entry = document.createElement('div');
  entry.className = 'entry';
  var h2 = document.createElement('h2');
  var a = document.createElement('span');
  a.textContent = date;
  a.addEventListener('click', function () {
    setPaginator(getDate(date));
  });
  var tweets = document.createElement('div');
  tweets.className = 'tweets';
  h2.appendChild(a);
  entry.appendChild(h2);
  entry.appendChild(tweets);
  return [entry, tweets];
}

function getDate(date) {
  return function (offset, limit, setTotal) {
    var path = 'date/' + date + '?offset=' + offset + '&limit=' + limit;
    callAPI(path, 'GET', null, function (onJSON, onError) {
      onJSON(function (json) {
        setTotal(Number(json['total']));
        showReturnToTimeline();
        showTimeline(json['tweets']);
      });
      onError(defaultError);
    });
  };
}

function createTweetCard(tweet, createdAt, imageIds) {
  var card = document.createElement('div');
  card.className = 'tweet';

  var time = document.createElement('div');
  time.className = 'tweet-time';
  time.innerHTML = formatTime(createdAt);
  card.appendChild(time);

  var text = document.createElement('div');
  text.className = 'tweet-text';
  text.textContent = tweet;
  autoLink(text);
  card.appendChild(text);

  if (imageIds.length > 0) {
    var images = document.createElement('div');
    images.className = 'tweet-images';

    for (var i = 0; i < imageIds.length; i++) {
      var a = document.createElement('a');
      a.href = '/images/' + imageIds[i];
      var image = document.createElement('img');
      image.src = a.href;
      a.appendChild(image);
      images.appendChild(a);
    }

    card.appendChild(images);
  }

  return card;
}

function insertTweet(tweet) {
  var createdAt = new Date(tweet['created_at']);
  var date = formatDate(createdAt);
  var tweets = getTweets(date);
  var tweet = createTweetCard(tweet['text'], createdAt, tweet['images']);

  if (tweets) {
    tweets.insertBefore(tweet, tweets.firstChild)
  } else {
    var elms = createEntry(date);
    var entry = elms[0];
    var tweets = elms[1];
    tweets.appendChild(tweet);
    timeline.insertBefore(entry, timeline.firstChild);
  }
}

var timeline, returnToTimelineCard;

function initTimeline () {
  timeline = document.getElementById('timeline');
  setPaginator(getTimeline);

  returnToTimelineCard = document.getElementById('return-to-timeline-card');

  var returnToTimeline = document.getElementById('return-to-timeline');

  returnToTimeline.addEventListener('click', function () {
    returnToTimelineCard.style.display = 'none';
    setPaginator(getTimeline);
  });
}

function showReturnToTimeline () {
  if (returnToTimelineCard.style.display == 'block') return;

  returnToTimelineCard.style.display = 'block';
}

function getTimeline (offset, limit, setTotal) {
  callAPI('timeline?offset=' + offset + '&limit=' + limit, 'GET', null, function (onJSON, onError) {
    onJSON(function (json) {
      setTotal(Number(json['total']));
      showTimeline(json['tweets']);
    });
    onError(defaultError);
  });
}

function showTimeline (tweets) {
  clearTimeline();

  var currentDate = '';
  var cards;
  var fragment = document.createDocumentFragment();

  for (var i = 0; i < tweets.length; i++) {
    var tweet = tweets[i];
    var createdAt = new Date(tweet['created_at']);
    var date = formatDate(createdAt);

    if (currentDate != date) {
      var tmp = createEntry(date);
      var entry = tmp[0];
      cards = tmp[1];
      fragment.appendChild(entry);
      currentDate = date;
    }

    var card = createTweetCard(tweet['text'], createdAt, tweet['images']);
    cards.appendChild(card);
  }

  timeline.appendChild(fragment);
}

function clearTimeline () {
  while (timeline.firstChild) {
    timeline.removeChild(timeline.firstChild);
  }
}
