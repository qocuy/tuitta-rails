var searchForm, searchBox;
var formData;

function initSearch () {
  searchForm = document.getElementById('search-form');
  searchBox = document.getElementById('search-box');

  searchForm.addEventListener('submit', function (e) {
    e.preventDefault();
    formData = new FormData(searchForm);
    setPaginator(search);
  });
}

function search (offset, limit, setTotal) {
  callAPI('search?offset=' + offset + '&limit=' + limit, 'POST', formData, function (onJSON, onError) {
    onJSON(function (json) {
      setTotal(Number(json['total']));
      showReturnToTimeline();
      showTimeline(json['tweets']);
    });

    onError(defaultError);
  });
}
