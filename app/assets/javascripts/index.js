//= require utils
//= require autoLink
//= require paginator
//= require timeline
//= require search
//= require form

window.addEventListener('load', function () {
  initPaginator();
  initForm();
  initSearch();
  initTimeline();
});
