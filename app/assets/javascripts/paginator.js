var paginator, firstButton, prevButton, nextButton, lastButton;
var numTweets = 30;
var totalTweets, currentPage, lastPage;
var onPaginate;

function initPaginator () {
  paginator = document.getElementById('paginator');
  firstButton = document.getElementById('page-first');
  prevButton = document.getElementById('page-prev');
  nextButton = document.getElementById('page-next');
  lastButton = document.getElementById('page-last');

  firstButton.addEventListener('click', function () {
    if (currentPage > 0) {
      paginate(0);
    }
  });

  prevButton.addEventListener('click', function () {
    if (currentPage > 0) {
      paginate(currentPage - 1);
    }
  });

  nextButton.addEventListener('click', function () {
    if (currentPage < lastPage) {
      paginate(currentPage + 1);
    }
  });

  lastButton.addEventListener('click', function () {
    if (currentPage < lastPage) {
      paginate(lastPage);
    }
  });
}

function paginate (page) {
  onPaginate(numTweets * page, numTweets, function (total) {
    document.body.scrollTop = 0;
    totalTweets = total;
    lastPage = Math.floor(totalTweets / numTweets);
    currentPage = page;
  });
}

// setPaginator :: (Total, Page, (Offset, Limit) -> ()) -> ()
function setPaginator (callback) {
  callback(0, numTweets, function (total) {
    totalTweets = total;
    currentPage = 0;
    lastPage = Math.floor(totalTweets / numTweets);
    onPaginate = callback;
  });
}
