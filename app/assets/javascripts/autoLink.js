function getText(node) {
  if (node.nodeType == Node.TEXT_NODE) {
    return node.nodeValue;
  }

  return node.textContent;
}

function setText(node, text) {
  if (node.nodeType == Node.TEXT_NODE) {
    node.nodeValue = text;
    return;
  }

  node.textContent = text;
}

function splitNode(parent, node, index) {
  if (node.nodeType == Node.TEXT_NODE) {
    return node.splitText(index);
  }

  var splitted = node.firstChild.splitText(index);
  var newNode = node.cloneNode();
  node.removeChild(splitted);
  newNode.appendChild(splitted);
  parent.insertBefore(newNode, node.nextSibling);
  return newNode;
}

function createLink(parent, url, chain) {
  var link = document.createElement('a');
  link.href = url;
  link.target = '_blank';

  var before = chain[chain.length - 1][0].nextSibling;

  for (var j = 0; j < chain.length; j++) {
    parent.removeChild(chain[j][0]);
    link.appendChild(chain[j][0]);
  }

  parent.insertBefore(link, before);

  return link;
}

function autoLink(tweet) {
  var chain = [];
  var textChain = '';
  var node = tweet.firstChild

  while (node) {
    var text = getText(node);
    chain.push([node, text.length]);
    textChain += text;

    if (match = textChain.match(/https?:\/\/[\w\/:%#\$&\?\(\)~\.=\+\-]+/)) {
      var url = match[0];
      // 先頭のURL以外の文字列、ノードを削除
      if (match.index > 0) {
        var textIndex = 0;
        var i = 0;
        while (textIndex + chain[i][1] < match.index) {
          textIndex += chain[i][1];
          i++;
        }
        //console.log('chainH', chain.map(function (x) { return getText(x[0]) }))
        if (match.index - textIndex < chain[i][1]) {
          var splitLength = match.index - textIndex;
          var splittedNode = splitNode(tweet, chain[i][0], splitLength);
          chain.splice(i + 1, 0, [splittedNode, chain[i][1] - splitLength]);
          //console.log('afterH', chain.map(function (x) { return getText(x[0]) }));
        }
        chain.splice(0, i + 1);
        //console.log('splicedH', chain.map(function (x) { return getText(x[0]) }))
        textChain = textChain.slice(match.index);
        node = chain[chain.length - 1][0];
      }

      // 末尾のURL以外の文字列、ノードを削除
      if (url.length < textChain.length) {
        var textIndex = 0;
        var i = 0;
        while (textIndex + chain[i][1] < url.length) {
          textIndex += chain[i][1];
          i++;
        }
        //console.log('chainT', chain.map(function (x) { return getText(x[0]) }))
        if (textIndex + chain[i][1] > url.length) {
          var splittedLength = textChain.length - url.length;
          var newLength = chain[i][1] - splittedLength;
          splitNode(tweet, chain[i][0], newLength);
          //console.log('afterT', chain.map(function (x) { return getText(x[0]) }))
        }
        chain.splice(i + 1);
        //console.log('splicedT', chain.map(function (x) { return getText(x[0]) }))
        node = createLink(tweet, url, chain);
        textChain = '';
        chain = [];
      } else if (!node.nextSibling) {
        createLink(tweet, textChain, chain);
      }
    }
    node = node.nextSibling;
  }
}
