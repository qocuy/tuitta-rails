function zeroPad(num) {
  return ('0' + num).slice(-2);
}

function formatDate(date) {
  return [date.getFullYear(), zeroPad(date.getMonth() + 1), zeroPad(date.getDate())].join('-');
}

function formatTime(date) {
  return [date.getHours(), date.getMinutes(), date.getSeconds()].map(zeroPad).join(':');
}

function getTweets(date) {
  var entry = document.getElementsByClassName('entry')[0];

  if (entry.firstChild.textContent == date) {
    return entry.lastChild;
  }
}

function callAPI(type, method, data, callback) {
  var xhr = new XMLHttpRequest();

  xhr.addEventListener('loadend', function () {
    var nothing = function (x) {};

    if (xhr.status == 200) {
      callback(function (onJSON) {
        onJSON(JSON.parse(xhr.responseText));
      }, nothing);
    } else {
      callback(nothing, function (onError) {
        onError(xhr.status);
      });
    }
  });

  xhr.open(method, '/api/' + type);
  xhr.send(data);
}

function defaultError (status) {
  alert('Error: ' + status.toString());
}
