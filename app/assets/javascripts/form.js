var tweetForm, submitTweet, tweetInput, uploadImage, uploadImageLabel;

function createTweet() {
  submitTweet.disabled = true;

  callAPI('create', 'POST', new FormData(tweetForm), function (onJSON, onError) {
    submitTweet.disabled = false;

    onJSON(function (json) {
      if (json['result']) {
         insertTweet(json['tweet'], json['images']);
        tweetInput.value = '';
        uploadImage.value = '';
      } else {
        alert("Error: " + json['messages']['text'].join("\n"));
      }
    });

    onError(defaultError);
  });
}

function initForm () {
  tweetForm = document.getElementById('tweet-form');
  tweetInput = document.getElementById('tweet-input');
  submitTweet = document.getElementById('submit-tweet');
  uploadImage = document.getElementById('upload-image');
  uploadImageLabel = document.getElementById('upload-image-label');

  tweetForm.addEventListener('submit', function (e) {
    e.preventDefault();
    createTweet();
  });

  tweetInput.addEventListener('keydown', function (e) {
    if (e.ctrlKey && e.keyCode == 13 && !submitTweet.disabled) {
      createTweet();
    }
  });

  uploadImage.addEventListener('change', function () {
    if (uploadImage.files.length > 0) {
      label.style.color = '#EF4836';
    } else {
      label.style.color = '#000000';
    }
  });
}
