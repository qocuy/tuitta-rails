class Tweet < ApplicationRecord
  validates :text, length: { in: 0..140 }

  def self.search(words)
    return [] if words.empty?

    initial_tweets = where('text like ?', '%' + words[0] + '%')

    words[1..-1].inject(initial_tweets) do |tweets, word|
      tweets.where('text like ?', '%' + word + '%')
    end
  end
end
