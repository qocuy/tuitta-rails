class ImagesController < ApplicationController
  def show
    image = Image.find(params[:id])
    send_data image.data, type: image.mime_type, disposition: 'inline'
  end
end
