class AuthController < ApplicationController
  def login
    if params[:password]
      if TuittaWeb::Application.config.password == params[:password]
        session[:login] = true
        redirect_to root_path
      else
        @message = 'パスワードが違います'
      end
    end
  end

  def logout
    session[:login] = false
    redirect_to action: 'login'
  end
end
