require 'uri'

class TimelineController < ApplicationController
  before_action :check_login

  def create
    has_images = params[:images].respond_to?(:each)

    if !has_images && (params[:tweet].nil? || params[:tweet].empty?)
      render json: { result: false, messages: { text: ['text is empty'] } }
      return
    end

    tweet = Tweet.new(text: params[:tweet])

    if tweet.save
      if has_images
        create_images(tweet)
      else
        render json: {
          result: true,
          tweet: {
            id: tweet.id, text: tweet.text, created_at: tweet.created_at, images: []
          }
        }
      end
    else
      render json: { result: false, messages: tweet.errors.messages }
    end
  end

  def index
  end

  def timeline
    render json: to_hash(Tweet.order(created_at: :desc))
  end

  def search
    words = params[:q].split(' ')
    render json: to_hash(Tweet.search(words).order(created_at: :desc))
  end

  def date
    splitted = params[:date_str].split('--', 2)

    if splitted.size == 1
      from = Time.parse(params[:date_str])
      to = from + 1.day
    elsif splitted[0].empty?
      from = nil
      to = Time.parse(splitted[1])
    elsif splitted[1].empty?
      from = Time.parse(splitted[0])
      to = nil
    else
      from = Time.parse(splitted[0])
      to = Time.parse(splitted[1])
    end

    if from.nil?
      tweets = Tweet.where('created_at <= ?', to)
    elsif to.nil?
      tweets = Tweet.where('created_at >= ?', from)
    else
      tweets = Tweet.where(created_at: from...to)
    end

    render json: to_hash(tweets.order(created_at: :desc))
  end

  private
  def create_images(tweet)
    images = []
    params[:images].each do |data|
      image = Image.create(tweet_id: tweet.id,
        mime_type: data.content_type, data: data.read)
      unless image
        images.each(&:destroy)
        render json: { result: false, messages: image.errors.messages }
        return
      end
      images << image
    end
    render json: { result: true, tweet: tweet, images: images.map(&:id) }
  end

  def check_login
    redirect_to controller: 'auth', action: 'login' unless session[:login]
  end

  def to_hash(query)
    offset = params[:offset] || 0
    limit = params[:limit] || 30

    rows = query.offset(offset).limit(limit)

    tweets = rows.map do |tweet|
      { id: tweet.id, text: tweet.text,
        created_at: tweet.created_at, images: get_image_ids(tweet.id) }
    end

    { total: query.count, tweets: tweets }
  end

  def get_image_ids(tweet_id)
    Image.where(tweet_id: tweet_id).select('id');
  end
end
